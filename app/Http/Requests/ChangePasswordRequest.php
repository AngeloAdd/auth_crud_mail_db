<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'oldPassword' => 'required',
            'password' => 'required|min:8|max:20|confirmed',
            'password_confirmation' => 'required|min:8|max:20'
        ];
    }
    public function messages()
    {
        return [
            'oldPassword.required' => 'old password is required',
            'password.min' => 'la password deve avere minimo :min caratteri.',
            'password.max' => 'la password deve avere massimo :max caratteri.',
            'password.confirmed' => "la password di conferma non coincide.",
            'password.required' => 'una nuova password è richiesta',
            'password_confirmation.min' => 'la password deve avere minimo :min caratteri.',
            'password_confirmation.max' => 'la password deve avere massimo :max caratteri.',
            'password_confirmation.required' => 'la conferma della password è richiesta'
        ];
    }
}
