<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:10|unique:articles',
            'body' => 'required|max:255'
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Il titolo è richiesto',
            'title.min' => 'Il titolo deve avere minimo :min caratteri.',
            'title.unique' => 'Esiste già un articolo con questo titolo.',
            'body.required' => "Il corpo dell'articolo è richiesto.",
            'body.max' => "L'articolo deve avere massimo :max caratteri."
        ];
    }
}
