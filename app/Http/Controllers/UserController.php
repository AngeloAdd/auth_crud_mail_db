<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified')->except('index', 'show');
    }

    public function index()
    {
        return view('users.index');
    }

    public function show()
    {
        return view('users.show');
    }
    public function edit()
    {
        return view('users.edit');
    }
    public function update(Request $request)
    {   
        Auth::user()->update($request->all());
        return redirect(route('users.edit'))->with('message', 'profilo aggiornato con successo');
    }
    public function updatePassword(ChangePasswordRequest $request)
    {   
        $hashedPassword = Auth::user()->password;
        if (Hash::check($request->oldPassword, $hashedPassword))
        {
            if (!Hash::check($request->password, $hashedPassword))
            {
                Auth::user()->update([
                    'password' => Hash::make($request->password)
                    ]);
                return redirect(route('users.edit'))->with('password_message', 'password aggiornata con successo');
            }
            else
            {
                return redirect(route('users.edit'))->with('password_error', 'la password deve essere diversa da quella precedente');
            }
        }
        else
        {
            return redirect(route('users.edit'))->with('password_error', 'la password attuale non corrisponde');
        }
    }
}
