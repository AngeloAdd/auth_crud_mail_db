<?php

namespace App\Http\Controllers;
use App\Models\Category;
use App\Models\Article;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified'])->except('show');
    }

    public function show(Category $category)
    {
        return view('categories.show', compact('category'));
    }

    public function destroy(Article $article, Category $category)
    {
        $category->articles()->detach($article);
        return redirect(route('articles.show', compact('article')));
    }
    public function store(Request $request, Article $article)
    {
        
        $categoriesArray = explode(' ',$request->category);

        foreach ($categoriesArray as $categoryItem)
        {   
            $categoryInstance = Category::firstOrCreate([
                'name' => trim(strtolower($categoryItem), ' ')
            ]);
            $article->categories()->attach($categoryInstance);
        }

        return redirect(route('articles.show', compact('article')));
    }
    
}
