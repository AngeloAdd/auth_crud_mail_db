<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreArticleRequest;
use App\Mail\ArticleDeleted;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewArticleStored;
use App\Mail\ArticleUpdated;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rules\Exists;
use Illuminate\Support\Facades\Gate;

class ArticleController extends Controller
{   
    public function __construct()
    {
        $this->middleware(['auth','verified'])->except('show','index');
        $this->middleware('author')->only('edit','update');
    }

    public function create() {
        return view('articles.create');
    }

    public function store(StoreArticleRequest $request) {
        /* Entra nell'utente va alla funzione articoli e attraverso la relazione crea un record. */
        $article = Auth::user()->articles()->create(
            [
                'title'=>$request->title,
                'body'=>$request->body,
                'image'=> $request->file('image') ? $request->file('image')->store('public/img/') : null
            ]
        );
        $categoriesArray = explode(' ',$request->category);

        foreach ($categoriesArray as $categoryItem)
        {   
            $categoryInstance = Category::firstOrCreate([
                'name' => trim(strtolower($categoryItem), ' ')
            ]);
            $article->categories()->attach($categoryInstance);
        }

        Mail::to('roccoadmin2001@dac.com')->send(new NewArticleStored($article));
        return redirect()->back()->with('message',"L'articolo è stato creato correttamente");
    }

    public function index()
    {
        $articles = Article::orderByDesc('id')->paginate(10);
        return view('articles.index', compact('articles'));
    }

    public function show(Article $article){
        $comments = $article->comments()->orderByDesc('id')->get();
        return view('articles.show', compact('article','comments'));
    }

    public function edit(Article $article){
        
        return view('articles.edit',compact('article'));
    }

    public function update(Article $article, StoreArticleRequest $request) {
        
        Mail::to('roccoadmin2001@dac.com')->send(new ArticleUpdated($article,$request->validated()));
        $article->update($request->validated());
        return redirect(route('articles.show', compact('article')))->with('message', 'Articolo modificato con successo ');
    }

    public function destroy(Article $article) {
        /* questo è un esempio di GatePolicy authorization sopra hai anche un esempio di middleware */
        $response = Gate::inspect('edit-article', $article);
        if ($response->allowed()) {
            return view('articles.edit',compact('article'));
        } else {
            return redirect(route('error.author'));
        }
        Mail::to('roccoadmin2001@dac.com')->send(new ArticleDeleted($article));
        $article->delete();
        return redirect(route('articles.index'))->with('message', 'Il tuo articolo è stato cancellato');
    }
}
