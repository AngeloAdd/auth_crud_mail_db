<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Article;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Article $article)
    {   
        
        $comment = Comment::create(
            ["body" => $request->body,
            "article_id" => $article->id,
            "user_id" => Auth::id()]
        );
        return redirect(route('articles.show', compact('article')));
    }
}
