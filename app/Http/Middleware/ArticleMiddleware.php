<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Article;

class ArticleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user_id = $next($request)->getOriginalContent()->getData()['article']->user_id;
        $id= $request->user()->id;
        if($id===$user_id){
            return $next($request);
        } else{
            return redirect(route('error.author'));
        }
    }
}
