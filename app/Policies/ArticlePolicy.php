<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Article;
use Illuminate\Auth\Access\Response;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function update(User $user, Article $article)
    {
        return $user->id === $article->user_id
                ? Response::allow()
                : Response::deny('You must be the author.');
    }
    public function edit(User $user, Article $article)
    {
        return $user->id === $article->user_id
                ? Response::allow()
                : Response::deny('You must be the author.');
    }
    public function destroy(User $user, Article $article)
    {
        return $user->id === $article->user_id
                ? Response::allow()
                : Response::deny('You must be the author.');
    }
}
