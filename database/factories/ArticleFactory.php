<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->unique()->realText(15),
            'body' => $this->faker->realText(255),
            'user_id' => $this->faker->biasedNumberBetween($min = 1, $max = 50, $function = 'sqrt'),
            'image' => $this->faker->imageUrl($width = 200, $height = 200)
        ];
    }
}
