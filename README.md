# ToDo List 


## Use branches in Git


### Aspetto grafico componenti
* navbar **V**:
 * distribuita per tutte le pagine **V**
 * form pagina register **fatta**
* gestione pagina login e registrati **fatta**
* pagina di avvenuto login **V**
* footer **V**

### Database Per Articoli
* database creato **V**
* migrazioni modello e controller creati **V**

### Gestione articoli Crud
* compilazione migration **V**
* routes e view **V**
* modello e controller **V**

### gestione grafica
* in più cercare di modificare anche l'aspetto grafico dell'articolo:
 * pagina index **V**
 * pagina show **V**

### Navigazione autenticati 
* solo pagine show e index visibili **V**
* tasto modifica e elimina e pagina per aggiunta articoli non caricabili **V**

### gestione articoli modifica auth
* tasto modifica solo per autore **V**
* tasto cancella solo per autore **V**

### validazione inserimento articolo
* controllare che il titolo abbia minimo dieci caratteri. required **V**
* controllare che il corpo abbia massimo 255 caratteri. required **V**
* inserire una forma di validazione visiva anche nella pagina che rispecchi gli errori **V**

### mail
* mail di verifica alla registrazione **V**
* mail all'admin di conferma per iscrizione **V**
* mail alla pubblicazione dell'articolo all'utente **V**
* update mail **V**
* cancellazione mail **V**
* avviso mail per commento e/o mi piace a autore articolo
* possibilità di scegliere nel profilo se ricevere tutte queste mail

### sezione commenti e voto
* possibilità di mettere stellina all'articolo con votazione e
 * database con voti 
 * risultato con media dei voti per un articolo dato
* impostare con crud sezione commenti **V**
* Impostare schermata per utenti non loggati che si può commentare solo se loggati **V**


### profilo utente
* visione articoli scritti e info **V**
* possibilità di modificare profilo:
 * Username Email Password (con conferma per Password)
* provare con table relation one to one con opzioni varie come tel e check per opzioni:
  * Relazione OneToOne
    * numero di telefono con CRUD
    * tabella opzioni user

### Creazione Middleware
* middleware customizzato per inserimento numero di Tel singolo per utente
* inserimento all'interno della pagina 

### Immagini
* possibilità di inserire immagini **V**
 * come immagine pura **V**
 * avatar

### gestione Articoli con relazione tabella User
* inserimento di funzione relazione nei modelli con One to Many **V**
* inserimento relazione nella migrazione con add per avere un riferimento **V**

### creazione categorie con relaione Many to Many
* creazione del controller per categorie **V**
 * possibilità di inserire una proposta di nuova categoria **V**
 * toggle categoria sotto articoli con possibilità di togliere e mettere **V**
 * ricerca per categoria

### ricerca articolo per parole chiave
* gestione del controller per la ricerca 
* valutare la possibilità di farlo con JS o controllare come farlo con laravel

### inserire possibilità di ordinare articoli
* BOOTSTRAP default **V**
* paginazione automatica **V**
* crea menù per scelta paginazione utente

### aggiunta ruoli per gestione
* utenti normali
* admin con pannello
 * pannello
 * middleware

### inserire un po di middleware
* auth e verified nei nuovi controller **V**
* condizionali nelle pagine **V**
* Creati Gate per autorizzazione in ArticleController **V**
* inserito Middleware per impedire a non autore di arrivare all'edit update dell'articolo **V**
* mantenuto Gate con Policy come esempio riferimento per impedire destroy. **V**
* creare middleare per Category e Comments e User.