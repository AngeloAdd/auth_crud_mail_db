<?php

use App\Http\Controllers\ArticleController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Usercontorller;
use App\Models\Article;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\StarController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* auth routes */
Auth::routes();
/* route per auth */
Route::get('/home', [HomeController::class, 'index'])->name('home');

/* route per verifica email */
Route::get('/email/verifica', [VerificationController::class, 'emailVerificationNotice'])->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', [VerificationController::class, 'emailVerificationHandler'])->name('verification.verify');

Route::post('/email/verification-notification', [VerificationController::class, 'emailVerificationSend'])->name('verification.send');




/* Route per gestione Articoli CRUD */
Route::get('/articoli/articolo/crea', [ArticleController::class, 'create'])->name('articles.create');
Route::post('/articoli/articolo/nuovo', [ArticleController::class, 'store'])->name('articles.store');

Route::get('/articoli/indice', [ArticleController::class, 'index'])->name('articles.index');
Route::get('/articoli/{article}', [ArticleController::class, 'show'])->name('articles.show');

Route::get('/articoli/{article}/modifica', [ArticleController::class, 'edit'])->name('articles.edit');
Route::put('/articoli/{article}/aggiorna', [ArticleController::class, 'update'])->name('articles.update');

Route::delete('/articoli/{article}/elimina', [ArticleController::class, 'destroy'])->name('articles.destroy');

/* Route creazione commento */
Route::post('/articoli/{article}/commento/nuovo', [CommentController::class, 'store'])->name('comments.store');

/* view article related to a category */
Route::get('/articoli/categoria/{category}', [CategoryController::class, 'show'])->name('categories.show');
/* Attach category to Article */
Route::post('/articoli/{article}/nuova/categoria', [CategoryController::class, 'store'])->name('categories.store');
/* Detach category from article */
Route::delete('/articoli/{article}/dissocia/{category}', [CategoryController::class, 'destroy'])->name('categories.destroy');

/* view index user profile */
Route::get('/utente/profilo', [UserController::class, 'index'])->name('users.index');
/* Articoli per utente */
Route::get('/utente/articoli', [UserController::class, 'show'])->name('users.show');
/* edit user profile */
Route::get('/utente/modifica/profilo', [UserController::class, 'edit'])->name('users.edit');
Route::put('/utente/aggiorna/informazioni/profilo', [UserController::class, 'update'])->name('users.update');
Route::put('/utente/aggiorna/password/profilo', [UserController::class, 'updatePassword'])->name('users.update.password');

/* Errore non autorizzato non sei autore */
Route::get('/errore/403', function () {
    return view('errors.author');
})->name('error.author');