
const categoryGear = document.getElementById('categoryGear');
let categoryBtn = document.querySelectorAll('[data-category]');
let categoryLink = document.querySelectorAll('[data-link]');
let addCategoryForm = document.getElementById('addCategoryForm')
let categoryBtnToggle = () => {
    return categoryBtn.forEach( el => {
        return el.classList.toggle('d-none');
    });
}

let categoryLinkToggle = () => {
    return categoryLink.forEach( el => {
        return el.classList.toggle('d-none') 
    });
}

let addCategoryToggle = () => {
    return addCategoryForm.classList.toggle('d-none');
}

categoryGear.addEventListener('click', () => {
    categoryBtnToggle();
    categoryLinkToggle();
    addCategoryToggle();
});