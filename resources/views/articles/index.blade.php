<x-layout>

<div class="container @if(session('message')) pt-2 @else pt-4 @endif">

    <div class="row d-flex justify-content-center">
        <div class="col-md-6 mb-1">
            @if (session('message'))
                <div class="alert alert-success">{{session('message')}}</div>
            @endif
        </div>
    </div>
    
    <div class="row justify-content-center">
    
    @foreach($articles as $article)
        <div class="col-md-3 m-3">
            <div class="card shadow border-success mb-3">
                <div class="card-header main-bg main-text title-font">By {{$article->user->name}}</div>
                <div class="card-body">
                    <h5 class="card-title">{{$article->title}}</h5>
                    <p class="card-text"><img class="img-fluid" src="{{$article->image ?? Storage::url($article->image)}}" alt=""></p>
                    <p class="card-text">{{substr($article->body,0,20)}}...</p>
                    <div class="card-text mb-3">
                        @foreach($article->categories as $category)
                            <a class="mx-2" href="{{route('categories.show', compact('category'))}}">{{$category->name}}</a>
                        @endforeach
                    </div>
                    <a class="btn sec-bg text-white text-decoration-none" href="{{route('articles.show', compact('article'))}}">
                        Leggi
                    </a>
                </div>
            </div>
        </div>
    @endforeach

    </div>

    <div class="row justify-content-center mt-3">
        <div class="col-md-6 d-flex justify-content-center main-text">
            {{ $articles->links() }}
        </div>
    </div>

</div>

<div style="margin-top: 400px;"></div>

</x-layout>