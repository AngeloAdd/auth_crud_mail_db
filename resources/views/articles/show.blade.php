<x-layout>

    <div class="container @if(session('message')) pt-2 @else pt-4 @endif">
    
        <div class="row d-flex justify-content-center">
            <div class="col-md-6 mb-1">
                @if (session('message'))
                    <div class="alert alert-success">{{session('message')}}</div>
                @endif
            </div>
        </div>

        <div class="row justify-content-center pt-5">
            <div class="col-10">
                <div class="card shadow border-success">
                    <div class="card-header main-bg main-text title-font">by {{$article->user->name}}</div>
                    <div class="card-body">
                        <h5 class="card-title">{{$article['title']}}</h5>
                        <p class="card-text"><img class="img-fluid" src="{{$article->image ?? Storage::url($article->image)}}" alt=""></p>
                        <p class="card-text">{{$article['body']}}</p>
                        <div class="card-text mb-3">
                            <p>Categorie:</p>
                            <div class="d-flex align-items-center">
                            @foreach($article->categories as $category)
                                <a data-link class="py-0 ms-2 main-text text-decoration-none fs-5"href="{{route('categories.show', compact('category'))}}">
                                    {{$category->name}}
                                </a>
                                <form action="{{route('categories.destroy', compact('article', 'category'))}}" method="POST">
                                @csrf
                                @method('DELETE')
                                    <button type="submit" class="d-none btn sec-bg rounded-pill text-white py-0 ms-2 main-text text-decoration-none fs-5" data-category href="{{route('categories.show', compact('category'))}}">
                                        {{$category->name}}
                                        <i class="text-white pt-4 bi bi-x fs-5"></i>
                                    </button>
                                </form>
                            @endforeach
                            <form id="addCategoryForm" class="ms-3 d-none" action="{{route('categories.store', compact('article'))}}" method="POST">
                                @csrf
                                <div class="d-flex">
                                    <input type="text" class="form-control" name="category">
                                    <button type="submit" class="text-white btn bg-danger ms-4">
                                        aggiungi
                                    </button>
                                </div>
                            </form>
                            @auth
                            @if(Auth::user()->id === $article->user->id)
                                <button id="categoryGear" class="pt-2 ms-0 btn">
                                    <i class="main-text fs-5 bi bi-gear-fill"></i>
                                </button>
                            @endif
                            @endauth
                            </div>
                        </div>
                        <div class="w-100 d-flex justify-content-center">
                            @auth
                                @if (Auth::user()->id === $article->user->id)
                                    <a class="btn bg-info ms-4 text-white" href="{{route('articles.edit', compact('article'))}}">
                                        Modifica
                                    </a>
                                    <form action="{{route('articles.destroy', compact('article'))}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                        <button type="submit" class="text-white btn bg-danger ms-4">
                                            Cancella
                                        </button>
                                    </form>
                                @endif
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8 my-4">
                @auth
                    <form action="{{route('comments.store', compact('article'))}}" method="POST">
                        @csrf
                        <div class="d-flex">
                            <input type="text" class="form-control" name="body">
                            <button type="submit" class="text-white btn bg-danger ms-4">
                                Commenta
                            </button>
                        </div>
                    </form>
                @else
                    <div class="main-text text-center fs-4">Per commentare devi essere loggato</div>
                @endauth
            </div>

            @foreach($comments as $comment)
                <div class="col-md-8 mt-2">
                    <div class="px-3 py-1 fw-bold main-bg shadow">
                        <p class="fs-5 mb-1">{{$comment->user->name}}</h4>
                        <p class="m-0">{{$comment['body']}}</p>
                        <p class="m-0">{{$comment['created_at']}}</p>
                    </div>
                </div>
            @endforeach

        </div>

    </div>

    <div style="margin-top: 400px;"></div>

</x-layout>