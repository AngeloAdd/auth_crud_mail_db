<x-layout>

    <div class="container mb-5 @if(session('message') || $errors->any()) pt-2 @else pt-4 @endif">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8 mb-1">
                @if($errors->any())
                    <div class="alert alert-danger" role="alert">
                        @foreach($errors->all() as $message)
                            <p class="mb-0">{{$message}}</p>
                        @endforeach
                    </div>
                @elseif (session('message'))
                    <div class="alert alert-success" role="alert">{{session('message')}}</div>
                @endif
            </div>
        </div>
    
        <div class="row justify-content-center">
            <div class="col-md-8">
            
            <div class="card shadow">
                <div class="card-header main-bg main-text title-font">Scrivi un bell'articolo</div>

                <div class="card-body">
                
                <form action="{{route('articles.update', compact('article'))}}" method="POST">
                @csrf
                @method('PUT')
                    <div class="row mb-3 d-flex align-items-center flex-column">
                        <div class="col-md-11">
                            <label for="title" class="form-label d-flex align-items-center ps-4">Modifica titolo dell'articolo</label>
                            <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="title" value="{{$article['title']}}">
                            @error('title')
                                <div class="mt-1 py-1 alert alert-danger">
                                    <p class="mb-0">{{$message}}</p>
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3 d-flex align-items-center flex-column">
                        <div class="col-md-11">
                            <label for="body" class="form-label ps-4">Modifica articolo</label>
                            <textarea class="form-control @error('body') is-invalid @enderror" name="body" id="body" rows="10">{{$article['body']}}</textarea>
                            @error('body')
                                <div class="mt-1 py-1 alert alert-danger">
                                    <p class="mb-0">{{$message}}</p>
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-3 row mb-0 ">
                        <div class="col-md-12 d-flex justify-content-center">
                            <button type="submit" class="btn btn-info text-white">
                                Modifica articolo
                            </button>
                        </div>
                    </div>
                </form>
            
                </div>
            </div>

            </div>
        </div>
    </div>


</x-layout>