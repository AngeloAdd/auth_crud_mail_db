<x-layout>

    <div class="container mb-5 @if(session('message') || $errors->any()) pt-2 @else pt-4 @endif">
        <div class="row d-flex justify-content-center">
            <div class="col-md-6 mb-1">
                @if($errors->any())
                    <div class="alert alert-danger" role="alert">
                        @foreach($errors->all() as $message)
                            <p class="mb-0">{{$message}}</p>
                        @endforeach
                    </div>
                @elseif (session('message'))
                    <div class="alert alert-success" role="alert">
                        {{session('message')}}
                    </div>
                @endif
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8">
            
            <div class="card shadow">
                <div class="card-header main-bg main-text title-font">Scrivi un bell'articolo</div>

                <div class="card-body">
                
                <form enctype="multipart/form-data" action="{{route('articles.store')}}" method="POST">
                    @csrf
                    <div class="row mb-3 d-flex align-items-center flex-column">
                        <div class="col-md-11">
                            <label for="title" class="form-label d-flex align-items-center ps-4">Titolo dell'articolo</label>
                            <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="title" value="{{old('title')}}">
                            @error('title')
                                <div class="mt-1 py-1 alert alert-danger">
                                    <p class="mb-0">{{$message}}</p>
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3 d-flex align-items-center flex-column">
                        <div class="col-md-11">
                            <label for="image" class="form-label ps-4">Inserisci un Immagine</label>
                            <input type="file" class="form-control @error('image') is-invalid @enderror" name="image" id="image" value="{{old('image')}}">
                            @error('image')
                                <div class="mt-1 py-1 alert alert-danger">
                                    <p class="mb-0">{{$message}}</p>
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3 d-flex align-items-center flex-column">
                        <div class="col-md-11">
                            <label for="body" class="form-label ps-4">Articolo</label>
                            <textarea class="form-control @error('body') is-invalid @enderror" name="body" id="body" rows="10">{{old('body')}}</textarea>
                            @error('body')
                                <div class="mt-1 py-1 alert alert-danger">
                                    <p class="mb-0">{{$message}}</p>
                                </div>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="row mb-3 d-flex align-items-center flex-column">
                        <div class="col-md-11">
                            <label for="category" class="form-label ps-4">Specifica le categorie divise da una virgola:</label>
                            <input type="text" class="form-control @error('image') is-invalid @enderror" name="category" id="category" value="{{old('category')}}">
                            @error('category')
                                <div class="mt-1 py-1 alert alert-danger">
                                    <p class="mb-0">{{$message}}</p>
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-3 row mb-0 ">
                        <div class="col-md-12 d-flex justify-content-center">
                            <button type="submit" class="btn sec-bg text-white">
                                Posta articolo
                            </button>
                        </div>
                    </div>
                </form>
            
                </div>
            </div>

            

            </div>
        </div>
    </div>


</x-layout>