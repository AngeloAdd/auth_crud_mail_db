<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$title ?? 'Auth&CRUD'}}</title>
    <link rel="stylesheet" href="/css/app.css">
    {{$style ?? ''}}
</head>
<body>
    <x-_navbar/>
    
    {{$slot}}
    
    <x-_footer/>
    <script src="/js/app.js" defer></script>
    @stack('scripts')
</body>
</html>