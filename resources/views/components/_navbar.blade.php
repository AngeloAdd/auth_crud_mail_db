<nav class="navbar shadow main-bg navbar-expand-lg py-3 px-3 mb-4">

    <!-- navbar main container -->
    <div class="container-fluid justify-content-end">

        <!-- Logo -->
        <a class="navbar-brand me-auto me-3" href="/">
            <i class=" display-6 main-text bi bi-back"></i>
        </a>

        <!-- DropDown Button for small vp -->
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="main-text fs-4 bi bi-chevron-double-down"></i>
        </button>

        <!-- collapsable menu for small vp -->
        <div class="ms-5 collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Main Links -->
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link-custom title-font main-text" href="{{route('articles.index')}}">Indice articoli</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link-custom title-font  main-text" href="{{route('articles.create')}}">Scrivi articolo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link-custom title-font main-text" href="#">Link</a>
                </li>
            </ul>

            <!-- auth navitems -->
            <div class="d-flex justify-content-center align-items-center">

                <!-- guest login and register -->
                @guest
                    <ul class="navbar-nav">
                        @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link nav-user-custom main-text" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link nav-user-custom main-text" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    </ul>
                
                <!-- authenticated -->
                @else

                    <a href="{{route('users.index')}}" class="avatar-custom me-3">
                        <img class="img-fluid rounded-pill" src="https://picsum.photos/200/200" alt="">
                    </a>
                    <!-- Example single danger button -->
                    <div class="btn-group">
                    <button type="button" class="btn sec-bg text-white dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                        {{Auth::user()->name}}
                    </button>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item  main-text" href="{{route('users.index')}}">Profilo Utente</a></li>
                        <li><a class="dropdown-item main-text" href="{{route('users.show')}}">I Miei Articoli</a></li>
                        <li><a class="dropdown-item main-text" href="{{route('users.edit')}}">Modifica Profilo</a></li>
                        <li><a class="dropdown-item main-text" href="#">Pannello Admin</a></li>
                        <li><hr class="dropdown-divider main-text"></li>
                        <li>
                        <button type="button" class="dropdown-item main-text" data-bs-toggle="modal" data-bs-target="#logOutModal">
                            Logout
                        </button>
                        
                        </li>
                    </ul>
                    </div>
                    
                     
                @endguest
            <!-- END auth navitems -->
            </div>

        <!--END  collapsable menu for small vp -->
        </div>

        <!-- searchbar and dropinput -->
        <div class="dropdown">
            <a class="btn" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
              <i class="main-text fs-5 bi bi-search"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-end ps-3 pe-2 py-2" aria-labelledby="dropdownMenuLink">
              <li class="dropdown-custom w-100 d-flex justify-content-center">
                <form class="d-flex">
                  <input class="dropdown-custom form-control me-2" type="search" placeholder="Cerca" aria-label="Search">
                  <button class="btn main-text" type="submit">
                      <i class="main-text bi bi-search"></i>
                  </button>
                </form>
              </li>              
            </ul>
        <!-- END searchbar and dropinput -->
        </div>

    <!-- END navbar main container -->
    </div>

</nav>

<!-- Modal -->
<div class="modal fade" id="logOutModal" tabindex="-1" aria-labelledby="logOutModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="logOutModalLabel">Log Out</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            Sei sicuro di voler fare logout?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                @csrf
                <button type="submit" class="btn text-white sec-bg">
                    Logout <i class="py-0 sec-text bi bi-box-arrow-in-right"></i>
                </button>
            </form>
        </div>
        </div>
    </div>
</div>