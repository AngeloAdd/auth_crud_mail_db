<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <h1>Buongiorno RoccoAdmin</h1>
    <p>è stato pubblicato un nuovo articolo nel suo blog di caccia e pernacchie:</p>
    <ul>
        <li>Titolo: {{$article['title']}}</li>
        <li>Testo dell'articolo: {{$article['body']}}</li>
        <li>Autore: {{$article->user->name}}</li>
    </ul>
    <p>Saluta Andonio</p>
</body>
</html>