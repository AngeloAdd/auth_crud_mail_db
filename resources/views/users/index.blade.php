<x-layout>

<div class="container w-100 pt-4">
    <div class="row w-100 justify-content-center align-items-center">
        <div class="col-md-8">
            <img src="http://picsum.photos/150/150" class="rounded-pill p-1 border border-3 border-success mx-auto d-block" alt="...">
        </div>
        <div class="mt-4 col-md-8 d-flex justify-content-center display-6">
            Ciao, {{Auth::user()->name}}
        </div>
    </div>
    
    <div class="row w-100 justify-content-center align-items-center">
        <div class="mt-4 col-md-8 d-flex justify-content-between fs-3 card-header main-text main-bg rounded-3">
            <p class="my-0">Informazioni Personali</p>
            <a href="{{route('users.edit')}}">
                <i class="bi bi-gear"></i>
            </a>
        </div>
        <div class="mt-2 col-md-8 d-flex justify-content-start flex-column main-text">
            <p class="mb-0"><span class="text-dark">Nome:</span> {{Auth::user()->name}}</p>
            <p><span class="text-dark">Email:</span> {{Auth::user()->email}}</p>
        </div>
    </div>
    <div class="row w-100 justify-content-center align-items-center">
        <div class="mt-4 col-md-8 d-flex fs-3 card-header main-text main-bg rounded-3">Articoli Scritti da Te</div>
    </div>
    <div class="row w-100 justify-content-center align-items-center pt-3">
        @foreach(Auth::user()->articles as $article)
            <div class="col-md-3 m-3">
                <div class="card shadow border-success mb-3">
                    <div class="card-header main-bg main-text title-font">By {{$article->user->name}}</div>
                    <div class="card-body">
                        <h5 class="card-title">{{$article->title}}</h5>
                        <p class="card-text"><img class="img-fluid" src="{{$article->image ?? Storage::url($article->image)}}" alt=""></p>
                        <p class="card-text">{{substr($article->body,0,20)}}...</p>
                        <div class="card-text mb-3">
                            @foreach($article->categories as $category)
                                <a class="mx-2" href="{{route('categories.show', compact('category'))}}">{{$category->name}}</a>
                            @endforeach
                        </div>
                        <a class="btn sec-bg text-white text-decoration-none" href="{{route('articles.show', compact('article'))}}">
                            Leggi
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
<div style="margin-top: 400px;"></div>



</x-layout>