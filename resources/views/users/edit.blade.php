<x-layout>

<div class="container mb-5 @if(session('message') || $errors->any()) pt-2 @else pt-4 @endif">
    <div class="row d-flex justify-content-center">
        <div class="col-md-8 d-flex justify-content-center display-6">
            Ciao, {{Auth::user()->name}}
        </div>
        <div class="mt-2 col-md-8 fs-5 d-flex justify-content-center main-text">
            Gestisci da qui le informazioni del tuo profilo.
        </div>
    </div>

    <div class="row d-flex justify-content-center">
        <div class="col-md-8 mb-1">
            @if($errors->any())
                <div class="alert alert-danger" role="alert">
                    @foreach($errors->all() as $message)
                        <p class="mb-0">{{$message}}</p>
                    @endforeach
                </div>
            @elseif (session('message'))
                <div class="alert alert-success" role="alert">{{session('message')}}</div>
            @endif
        </div>
    </div>
    <div class="row w-100 justify-content-center align-items-center">
        <div class="mt-4 col-md-8 d-flex justify-content-between fs-3 card-header main-text main-bg rounded-3">
            <p class="my-0">Modifica Avatar</p>
            <a href="{{route('users.edit')}}">
                <i class="bi bi-gear"></i>
            </a>
        </div>
        <div class="col-md-8 mt-4">
            <img src="http://picsum.photos/150/150" class="rounded-pill p-1 border border-3 border-success mx-auto d-block" alt="...">
        </div>
        
    </div>
    
    <div class="row w-100 justify-content-center align-items-center">
        <div class="mt-4 col-md-8 d-flex justify-content-between fs-3 card-header main-text main-bg rounded-3">
            <p class="my-0">Informazioni Personali</p>
            <a href="{{route('users.edit')}}">
                <i class="bi bi-gear"></i>
            </a>
        </div>
        <div class="mt-2 col-md-8 d-flex justify-content-start flex-column main-text">
            <div class="container">
                <form action="{{route('users.update')}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row mb-3 d-flex align-items-center flex-column">
                        <div class="col-md-11">
                            <label for="name" class="form-label d-flex align-items-center ps-4">Modifica Nome</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" value="{{Auth::user()->name}}">
                            @error('name')
                                <div class="mt-1 py-1 alert alert-danger">
                                    <p class="mb-0">{{$message}}</p>
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3 d-flex align-items-center flex-column">
                        <div class="col-md-11">
                            <label for="email" class="form-label ps-4">Modifica Email</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" value="{{Auth::user()->email}}">
                            @error('email')
                                <div class="mt-1 py-1 alert alert-danger">
                                    <p class="mb-0">{{$message}}</p>
                                </div>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

    
            

    <div class="row w-100 justify-content-center align-items-center">
        <div class="mt-4 col-md-8 d-flex justify-content-between fs-3 card-header main-text main-bg rounded-3">
            <p class="my-0">Modifica Password</p>
            <a href="{{route('users.edit')}}">
                <i class="bi bi-gear"></i>
            </a>
        </div>

        <div class="mt-2 col-md-8 d-flex justify-content-start flex-column main-text">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-md-12 d-flex justify-content-center mb-1">
                        @if (session('password_message'))
                            <div class="alert alert-success" role="alert">{{session('password_message')}}</div>
                        @elseif (session('password_error'))
                            <div class="alert alert-danger" role="alert">{{session('password_error')}}</div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="container">
                <form action="{{route('users.update.password')}}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="row g-3 align-items-center">
                        <div class="col-3">
                            <label for="oldPassword" class="col-form-label">Vecchia Password</label>
                        </div>
                        <div class="col-3">
                            <input name="oldPassword" type="password" id="oldPassword" class="form-control @error('name') is-invalid @enderror" aria-describedby="oldPasswordHelpInline">
                        </div>
                        <div class="col-3">
                            <span id="oldPasswordHelpInline" class="form-text">
                            Must be 8-20 characters long.
                            </span>
                        </div>
                        <div class="col-m-12">
                            @error('oldPassword')
                                <div class="mt-1 py-1 alert alert-danger">
                                    <p class="mb-0">{{$message}}</p>
                                </div>
                            @enderror
                        </div>
                    </div>

                    <div class="row g-3 align-items-center">
                        <div class="col-3">
                            <label for="password" class="col-form-label">Nuova Password</label>
                        </div>
                        <div class="col-3">
                            <input name="password" type="password" id="password" class="form-control @error('name') is-invalid @enderror" aria-describedby="passwordHelpInline">
                        </div>
                        
                        <div class="col-m-12">
                            @error('password')
                                <div class="mt-1 py-1 alert alert-danger">
                                    <p class="mb-0">{{$message}}</p>
                                </div>
                            @enderror
                        </div>
                    </div>

                    <div class="row g-3 align-items-center">
                        <div class="col-3">
                            <label for="password_confirmation" class="col-form-label">Conferma Password</label>
                        </div>
                        <div class="col-3">
                            <input name="password_confirmation" type="password" id="password_confirmation" class="form-control @error('name') is-invalid @enderror" aria-describedby="password_confirmationHelpInline">
                        </div>
                        
                        <div class="col-m-12">
                            @error('password_confirmation')
                                <div class="mt-1 py-1 alert alert-danger">
                                    <p class="mb-0">{{$message}}</p>
                                </div>
                            @enderror
                        </div>
                    </div>

                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div style="margin-top: 400px;"></div>

</x-layout>