@extends('layouts.app')

@section('content')
<div class="container mt-5 pt-4">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow">
                <div class="card-header main-bg main-text title-font">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="mb-3 row">
                            <label for="name" class="col-md-4 form-label d-flex align-items-center ps-4">{{ __('Name') }}</label>
                            <div class="col-md-6">
                                
                                <input type="name" class="form-control @error('name') is-invalid @enderror" id="name" name="name" aria-describedby="nameHelp">
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>                         
                        </div>

                        <div class="mb-3 row">
                            <label for="email" class="col-md-4 form-label d-flex align-items-center ps-4">{{ __('E-Mail Address') }}</label>
                            <div class="col-md-6">
                                
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" aria-describedby="emailHelp">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                               </div>                     
                               <div id="emailHelp" class="col-md-6 offset-md-4 form-text">We'll never share your email with anyone else.</div>    
                        </div>

                        <div class="mb-3 row">
                            <label for="password" class="col-md-4 form-label d-flex align-items-center ps-4">{{ __('Password') }}</label>
                            <div class="col-md-6">
                                
                                <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" aria-describedby="passwordHelp">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>                         
                        </div>


                        <div class="mb-3 row">
                            <label for="password-confirm" class="col-md-4 form-label d-flex align-items-center ps-4">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="mb-3 row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn sec-bg text-white">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
