@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-4">
        <div class="col-md-8">
                @if (session('message'))
                    <div class="alert alert-success" role="alert">
                        {{session('message')}}
                    </div>
                @endif
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header main-bg main-text title-font">Verifica Il Tuo Indirizzo Email</div>

                <div class="card-body d-flex flex-column align-items-center">
                    

                    <p class="mb-0">Prima di procedere, verifica la tua email al link che ti abbiamo inviato.</p>
                    <p class="mb-1">Se non hai ricevuto una mail,</p>
                    <form class="d-inline" method="POST" action="{{ route('verification.send') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">Clicca qui per richiedere un nuovo invio.</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
